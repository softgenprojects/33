import { Box, VStack, Heading, Button, Text, useColorModeValue, Center } from '@chakra-ui/react';
import { BrandLogo } from '@components/BrandLogo';

const Home = () => {
  const bgColor = useColorModeValue('gray.50', 'gray.800');
  const textColor = useColorModeValue('gray.800', 'whiteAlpha.900');

  return (
    <Center bg={bgColor} color={textColor} minH="100vh" py="12" px={{ base: '4', lg: '8' }}>
      <VStack spacing="8" align="stretch" maxW={{ lg: '2xl' }} mx="auto">
        <BrandLogo />
        <Heading as="h1" size="xl" textAlign="center">
          Fahrschule.live
        </Heading>
        <Text textAlign="center" fontSize={{ base: 'md', md: 'lg' }}>
          Verwalte Deine Fahrschule effizient, zeitsparend und ortsunabhängig mit Fahrschule.live.
        </Text>
        <Text textAlign="center" fontSize={{ base: 'md', md: 'lg' }}>
          Unser Ziel: Wir helfen Fahrschulinhabern Zeit und Geld durch automatisierte und digitalisierte Verwaltungsprozesse zu sparen.
        </Text>
        <Text textAlign="center" fontSize={{ base: 'md', md: 'lg' }}>
          Unsere Softwarelösungen wurden von Fahrlehrern für Fahrlehrer entwickelt.
        </Text>
        <Button colorScheme="pink" size="lg" _hover={{ bg: 'pink.500', transform: 'scale(1.05)' }} _active={{ bg: 'pink.600', transform: 'scale(0.95)' }}>
          Jetzt starten
        </Button>
      </VStack>
    </Center>
  );
};

export default Home;
